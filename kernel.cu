
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
//#include <cub/cub.cuh>

#include <stdio.h>
#include <cstdint>
#include <random>
#include <string>
#include <time.h>
#include <iostream>
using namespace std;
#define N 12800

__device__ __host__ uint32_t Jenkins(char* key, int length);
__global__ void HashKernel(int *c, char *a);
cudaError_t HashPrecision(int *c, char *a, long f);


__device__ __host__ uint32_t Jenkins(char* key, int length) {
	int i = 0;
	uint32_t hash = 0;
	while (i != length) {
		hash += key[i++];
		hash += hash << 10;
		hash ^= hash >> 6;
	}
	hash += hash << 3;
	hash ^= hash >> 11;
	hash += hash << 15;
	return hash;
}


__global__ void HashKernel(int *c, char *a)
{
	int i = threadIdx.x;
	//hashing input with jenkins hash:
	int s = c[i+1] - c[i];
	char * f = (char*)malloc(s * sizeof(char));
	for (int gm = 0; gm < s; gm++){
		f[gm] = a[c[i]+gm];
	}
	c[i] = Jenkins(f, s);
}

// __global__ void testKernel(int *d_in, int *d_out)
// {
// 	//int i = threadIdx.x;
// 	//hashing input with jenkins hash:
// 	//int s = sizeof(a[i])/sizeof(char*);
// 	//c[i] = Jenkins(a[i], s);
    
//     // Specialize BlockLoad, BlockStore, and BlockRadixSort collective types
//     typedef cub::BlockLoad<int*, BLOCK_THREADS, ITEMS_PER_THREAD, BLOCK_LOAD_TRANSPOSE> BlockLoadT;
//     typedef cub::BlockStore<int*, BLOCK_THREADS, ITEMS_PER_THREAD, BLOCK_STORE_TRANSPOSE> BlockStoreT;
//     typedef cub::BlockRadixSort<int, BLOCK_THREADS, ITEMS_PER_THREAD> BlockRadixSortT;
//         // Allocate type-safe, repurposable shared memory for collectives
//     __shared__ union {
//         typename BlockLoadT::TempStorage       load; 
//         typename BlockStoreT::TempStorage      store; 
//         typename BlockRadixSortT::TempStorage  sort;
//     } temp_storage; 
//         // Obtain this block's segment of consecutive keys (blocked across threads)
//     int thread_keys[ITEMS_PER_THREAD];
    
//     int block_offset = blockIdx.x * (BLOCK_THREADS * ITEMS_PER_THREAD);      
    
//     BlockLoadT(temp_storage.load).Load(d_in + block_offset, thread_keys);
        
//     __syncthreads();    // Barrier for smem reuse
//         // Collectively sort the keys
//     BlockRadixSortT(temp_storage.sort).Sort(thread_keys);
//     __syncthreads();    // Barrier for smem reuse
//         // Store the sorted segment 
//     BlockStoreT(temp_storage.store).Store(d_out + block_offset, thread_keys);
// }

char* random_string(int & temp){
	std::random_device rd;
	std::string str("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
	std::mt19937 generator(rd());
	
	std::uniform_int_distribution<int> dis(3, 12);
	std::uniform_int_distribution<int> dis2(0, 63);

	int length = dis(generator);
	temp = length;
	char * res = (char*)malloc(length * sizeof(char));
	int counter  =0;
	while (length--){
		res[counter++] = str[dis2(generator)];
	}

  

	return res;
}


//for hash percision, char counter
int countdistinct(char **a, int num){
	int result = 0;
	for(int i = 0; i < num; i++){
		for(int j = 0; j < i; j++){
			if(a[i] != a[j]){
				result++;
			}
		}
	}
	return result;
}

//for hash percision, int counter
int countdistinct(int *a, int num){
	int result = 0;
	for(int i = 0; i < num; i++){
		for(int j = 0; j < i; j++){
			if(a[i] != a[j]){
				result++;
			}
		}
	}
	return result;
}

int main()
{
	int *c = (int*)malloc(N * sizeof(int));
	int *sizes = (int*)malloc((N+1) * sizeof(int));
	char **a = (char**)malloc(N * sizeof(char*));


	//generating random input for test
	long finalSize = 0;
	sizes[0] = 0;
	for (int i = 1; i < N; ++i){
		int tmp = 0;
		a[i] = random_string(tmp);
		sizes[i] = tmp + sizes[i-1];
		finalSize = finalSize + tmp;
		//cout << a[i];
		//cout << " which has a length of: " << sizes[i] << endl;
	}
	cout << "strings are created.\n";
	cout << finalSize << endl;

	char * all = (char*)malloc(finalSize * sizeof(char));
	int cnt =0;
	for (int j = 1; j < N; j++){
		//cout << "SIZES are " << sizes[j-1] << " " << sizes[j] << endl;
		for(int g = sizes[j-1]; g < sizes[j]; g++){
			
			//cout << a[j][g] << endl;
			all[cnt++] = a[j][g-sizes[j-1]];
			//cout << all[cnt-1] << endl;
		}
	}
	cout << "all is " << all <<endl;

	// Add vectors in parallel.
	cudaError_t cudaStatus = HashPrecision(c, all, finalSize);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "addWithCuda failed!");
		return 1;
	}

	//printf("{1,2,3,4,5} + {10,20,30,40,50} = {%d,%d,%d,%d,%d}\n",
	//    c[0], c[1], c[2], c[3], c[4]);

	// cudaDeviceReset must be called before exiting in order for profiling and
	// tracing tools such as Nsight and Visual Profiler to show complete traces.
	cudaStatus = cudaDeviceReset();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceReset failed!");
		return 1;
	}

	return 0;
}

// Helper function for using CUDA to add vectors in parallel.
cudaError_t HashPrecision(int *c, char *a, long fs)
{
	int *dev_c = 0;
	char *dev_a = 0;

	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		goto Error;
	}

	// Allocate GPU buffers for three vectors (two input, one output)    .
	cudaStatus = cudaMalloc((void**)&dev_c, N * sizeof(int));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&dev_a, fs * sizeof(char));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	// Copy input vectors from host memory to GPU buffers.
	cudaStatus = cudaMemcpy(dev_a, a, fs * sizeof(char), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

	cudaStatus = cudaMemcpy(dev_c, c, N * sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}




	// Launch a kernel on the GPU with one thread for each element.
	HashKernel <<< 128 , N / 128 >>> (dev_c, dev_a);

	//testKernel <<< 128 , N / 128 >>> (fllatenned, gpuoutput)
	//int *d_in = ...; 
	//int *d_out = ...;
	//int num_blocks = ...;
	//BlockSortKernel<128, 16><<<num_blocks, 128>>>(d_in, d_out); 


	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
		goto Error;
	}

	// Copy output vector from GPU buffer to host memory.
	cudaStatus = cudaMemcpy(a, dev_a, N * sizeof(int), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

Error:
	cudaFree(dev_c);
	cudaFree(dev_a);

	//int checksum = gpuoutput/countdistinct(a[i]);
	//cout << "percision of calculations is:" << checksum << endl;



	//delete[] inpdata;
	return cudaStatus;
}
